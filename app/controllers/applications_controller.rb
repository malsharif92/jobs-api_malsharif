class ApplicationsController < ApplicationController
before_action :set_post
  before_action :set_post_application, only: [:show, :update, :destroy]


  # GET /posts/:post_id/applications
  def index
    json_response(@post.applications)
  end

  # GET /posts/:post_id/applications/:id
  def show
    json_response(@application)
  end

  # POST /posts/:post_id/applications
  def create
    @post.applications.create!(post_params)
    json_response(@post, :created)
  end

  # PUT /posts/:post_id/applications/:id
  def update
    @post.update(application_params)
    head :no_content
  end

  # DELETE /posts/:post_id/applications/:id
  def destroy
    @application.destroy
    head :no_content
  end

  private

  def application_params
    params.permit(:created_by)
  end

  def set_post
    @post = Post.find(params[:post_id])
  end

  def set_post_application
    @application = @post.applications.find_by!(id: params[:id]) if @post
  end
end

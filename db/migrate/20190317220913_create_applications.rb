class CreateApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :applications do |t|
      t.boolean :seen, :null => false, :default => 'P'
      t.string :created_by, :null => false
      t.references :post, foreign_key: true

      t.timestamps
    end
  end
end

# spec/factories/applications.rb
FactoryBot.define do
  factory :application do
    seen false
    created_by { Faker::Number.number(10) }
    post_id nil
  end
end
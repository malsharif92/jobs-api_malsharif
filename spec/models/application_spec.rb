require 'rails_helper'

RSpec.describe Application, type: :model do
  # Association test
    # ensure an application record belongs to a single post record
    it { should belong_to(:post) }
    # Validation test
    # ensure column name is present before saving
    it { should validate_presence_of(:created_by) }
end

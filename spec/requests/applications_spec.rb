# spec/requests/applications_spec.rb
require 'rails_helper'

RSpec.describe 'Applications API' do
  # Initialize the test data
  let!(:post) { create(:post) }
  let!(:applications) { create_list(:applications, 20, post_id: post.id) }
  let(:post_id) { post.id }
  let(:id) { applications.first.id }

  # Test suite for GET /posts/:post_id/applications
  describe 'GET /posts/:post_id/applications' do
    before { get "/posts/#{post_id}/applications" }

    context 'when post exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all post applications' do
        expect(json.size).to eq(20)
      end
    end

    context 'when pst does not exist' do
      let(:post_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Post/)
      end
    end
  end

  # Test suite for GET /posts/:post_id/applications/:id
  describe 'GET /posts/:post_id/applications/:id' do
    before { get "/posts/#{post_id}/applications/#{id}" }

    context 'when post application exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the application' do
        expect(json['id']).to eq(id)
      end
    end

    context 'when post application does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Application/)
      end
    end
  end

  # Test suite for PUT /posts/:post_id/applications
  describe 'POST /posts/:post_id/applications' do
    let(:valid_attributes) { { created_by: '1' } }

    context 'when request attributes are valid' do
      before { post "/posts/#{post_id}/applications", params: valid_attributes }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/posts/#{post_id}/applications", params: {} }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Created by can't be blank/)
      end
    end
  end

  # Test suite for PUT /posts/:post_id/applications/:id
  describe 'PUT /posts/:post_id/applications/:id' do
    let(:valid_attributes) { { created_by: '2' } }

    before { put "/posts/#{post_id}/applications/#{id}", params: valid_attributes }

    context 'when application exists' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it 'updates the application' do
        updated_application = Application.find(id)
        expect(updated_application.name).to match(/2/)
      end
    end

    context 'when the application does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Application/)
      end
    end
  end

  # Test suite for DELETE /posts/:id
  describe 'DELETE /posts/:id' do
    before { delete "/posts/#{post_id}/applications/#{id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end